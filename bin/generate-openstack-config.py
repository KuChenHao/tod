import os
import os_client_config
import json
import yaml

openstack_config_path = "/etc/openstack"
target = "%s/clouds.yaml" % openstack_config_path

if not os.path.isdir(openstack_config_path):
	os.mkdir(openstack_config_path)

cloud_config = os_client_config.OpenStackConfig()

clouds = {}
for cloud in cloud_config.get_all_clouds():
	clouds[cloud.name] = cloud.config
	clouds[cloud.name]["profile"] = cloud.name

config_str = json.dumps({"clouds": clouds})
config = yaml.dump(yaml.load(config_str), default_flow_style=False).encode('ascii')

with open(target, 'w') as f:
	f.write(config)
