# TOD - Tiny OpenStack Deployment

TOD is an OpenStack deployment by using Ansible 2.2.0.
TOD support Ubuntu 14.04 and 16.04.

# Prerequiresuit

## Install Ansible 2.2.0

Ansible 2.2.0 is RC version now, so we need install it with source code.

Install steps:

- Install development tools
  ```
  sudo apt-get install build-essential checkinstall
  ```

- Download source code
  ```
  wget http://releases.ansible.com/ansible/ansible-2.2.0.0-0.1.rc1.tar.gz
  tar zxf ansible-2.2.0.0-0.1.rc1.tar.gz
  ```

- Build and install ansible package
  ```
  sudo apt-get build-dep ansible
  cd ansible-2.2.0.0
  sudo checkinstall make install
  ```

The command `checkinstall` will auto install package.
You can execute command `dpkg` to verify:
```
dpkg -l | grep ansible
```

Don't forget configure ansible configuration: `/etc/ansible/ansible.cfg`
(Setup roles_path)

# Installtion

**YOU need switch to root privileges**

```
sudo su -
```

## Configure environment variables

- Edit `adminrc`
- Activate environment variables
  ```
  source adminrc
  ```

## Generate cloud config:

- Generate cloud config:
  ```
  python generate-cloud-config.py
  ```

## Other configuration:

- Create soft-link to MySQL administrator client config

  ```
  ln -s /etc/mysql/debian.cnf ~/.my.cnf
  ```

  This is an invaild soft-link because MariaDB isn't install yet. But this step is required for installation.

- Install Python-Shade
  ```
  pip install shade==1.6.0
  ```

## Install OpenStack

```
ansible-playbook controller.yml
ansible-playbook compute.yml
```
