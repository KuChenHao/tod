[DEFAULT]
dhcpbridge_flagfile=/etc/nova/nova.conf
dhcpbridge=/usr/bin/nova-dhcpbridge
logdir=/var/log/nova
state_path=/var/lib/nova
lock_path=/var/lock/nova
force_dhcp_release=True
libvirt_use_virtio_for_bridges=True
verbose=True
ec2_private_dns_show_ip=True
api_paste_config=/etc/nova/api-paste.ini
enabled_apis=ec2,osapi_compute,metadata
rpc_backend = rabbit
auth_strategy = keystone

my_ip = {{ ansible_default_ipv4.address }}

use_neutron = True
firewall_driver = nova.virt.firewall.NoopFirewallDriver

[api_database]
connection = mysql+pymysql://nova:{{ openstack_db_passwd }}@{{ groups['controller'][0] }}/nova_api

[database]
connection = mysql+pymysql://nova:{{ openstack_db_passwd }}@{{ groups['controller'][0] }}/nova

[oslo_messaging_rabbit]
rabbit_host = {{ groups["controller"][0] }}
rabbit_userid = openstack
rabbit_password = {{ rabbitmq_passwd }}

[keystone_authtoken]
auth_uri   = http://{{ groups["controller"][0] }}:5000
auth_url   = http://{{ groups["controller"][0] }}:35357
memcached_servers = {{ groups["controller"][0] }}:11211
auth_type = password
project_domain_name = default
user_domain_name = default
project_name = service
username = nova
password = {{ openstack_service_passwd }}

[vnc]
vncserver_listen = 0.0.0.0
vncserver_proxyclient_address = $my_ip
{% if ansible_hostname in groups["compute"] %}
enabled = True
novncproxy_base_url = http://{{ hostvars[groups['controller'][0]].ansible_default_ipv4.address }}:6080/vnc_auto.html
{% endif %}

[glance]
api_servers = http://{{ groups["controller"][0] }}:9292

[oslo_concurrency]
lock_path = /var/lib/nova/tmp

[neutron]
url = http://{{ groups["controller"][0] }}:9696
auth_url = http://{{ groups["controller"][0] }}:35357
auth_type = password
project_domain_name = default
user_domain_name = default
region_name = RegionOne
project_name = service
username = neutron
password = {{ openstack_service_passwd }}
service_metadata_proxy = True
metadata_proxy_shared_secret = {{ metadata_secret }}
